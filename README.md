# Projekty Oculus Go
Zbiór zdjęć oraz wideo projektów wykonanych na gogle Oculus Go.

## Spis

- [Spółki Grupy PZU](#spółki-grupy-pzu)
- [MetroVR](#metrovr)

#

### Spółki Grupy PZU
Projekt realizowany dla PZU. Na filmie jeden z pierwszych testów, przedstawiający początek tutorialu (obsługi kontrolera) oraz system poruszania się po makiecie.

<a align='center' href="https://youtu.be/mKJL_gUjAig?t=21" target="_blank"><img src="http://img.youtube.com/vi/mKJL_gUjAig/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>

#

### MetroVR

Gra [MetroVR - FPS Shooter](https://www.oculus.com/experiences/go/2278028978943163/?locale=pl_PL) przeznaczona na gogle Oculus Go (na Questa nie chcieli wypuścić, ze względu na: "zbyt krótko jesteśmy na rynku gier"). Odnośnik do szerszych informacji na [EpicVr.pl](https://epicvr.pl/pl/metro-vr-fps-shooter-vr-game/).

<a align='center' href="https://youtu.be/fLlmonRjoEU" target="_blank"><img src="http://img.youtube.com/vi/fLlmonRjoEU/0.jpg" 
alt="wideoprezentujaca" border="10" /></a>
